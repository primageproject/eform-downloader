# eform-downloader

Script to download eforms and create the dataset directory with symbolic links to the datalake.

## Requirements

Pyhton 3.8.

## Example of use
```
python downloadEForms.py -h
```
This shows the help page with an example of use at the end of description.

## DOC for developers

### Operations with curl (Windows cmd format)

#### Login

```
curl -i -k -X POST "https://primage.quibim.com/api/account/login" ^
  -H "Content-Type: application/json;charset=UTF-8" ^
  --data-binary "{\"email\":\"quibimUser@example.com\",\"password\":\"xxxxxxxx\"}"

HTTP/1.1 200 OK
{"user":"quibimUser@example.com","acceptedTerms":true,"token":"eyJ0eXAiOi...YN4s"}
```

#### Get eForm

```
curl -i -k -X GET https://primage.quibim.com/api/subject/5fcfcdec5f637600123248ff ^
  -H "AuthorizationQuiBim: QuiBim eyJ0eXAiOi...YN4s"

{"randomized":"2020-11-23T11:06:50.602Z","studies":[],"_id":"5fcfcdec5f637600123248ff","code":"test4","user":"5c405497eeb5d70d84a7a68d","trial":{"manual":[],"encrypt_tags":[{"tag":"x00100010","value":"code"},"x00100020","x00200010"],"centers_code":[],"timepoints":[{"code":"Diagnosis"},{"code":"Treatment evaluation"}],"_id":"5c40540beeb5d70d84a7a688","name":"Generic","code":"Generic","folder":"Quibim_Generic","sponsor":"QUIBIM","subject_form":"/subject_form/custom_templates/covid_avi-6.html","createdAt":"2017-12-18T10:20:16.996Z","updatedAt":"2021-01-13T18:15:33.304Z","template":"","default_timepoint":"Diagnosis"},"form":{"content":{"age":"24"},"status":{"id":2,"state":"Complete"}},"createdAt":"2020-12-08T19:03:08.287Z","updatedAt":"2021-02-16T14:35:22.780Z","__v":0}

```

