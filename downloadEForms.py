#!/usr/bin/env python
import os
import json
import argparse
import http.client
from getpass import getpass
import ssl

EFORMS_FILE_NAME = 'eForms.json'

def login(connection, user, password):
    payload = '{"email":"'+user+'","password":"'+password+'"}'
    headers = {'Content-Type': 'application/json;charset=UTF-8'}
    connection.request("POST", "/api/account/login", payload, headers)
    res = connection.getresponse()
    httpStatusCode = res.status
    msg = res.read()  # whole response must be readed in order to do more requests using the same connection
    if httpStatusCode != 200:
        print('Login error. Code: %d %s' % (httpStatusCode, res.reason))
        print(msg)
        return ''
    else:
        print('Login success.')
        response = json.loads(msg)
        #print(response)
        return response['token']

def upload(eForm, connection, token, trial):
    payload = '{"project":"'+trial+'","code":"'+eForm['subject_code']+'","eform":{"status":{"id": 2,"state":"Complete"},"content":'+json.dumps(eForm['content'])+'}}'
    print(payload)
    headers = {'AuthorizationQuiBim': 'QuiBim '+token, 
               'Content-Type': 'application/json;charset=UTF-8'}
    connection.request("POST", "/api/subject/eform", payload, headers)
    res = connection.getresponse()
    httpStatusCode = res.status
    msg = res.read()  # whole response must be readed in order to do more requests using the same connection
    if httpStatusCode != 200:
        print('Error uploading eForm. Code: %d %s' % (httpStatusCode, res.reason))
        print(msg)
        return False
    else:
        print('.', end='')
        return True
    
def downloadSubject(subjectId, connection, token):
    payload = ''
    headers = {'AuthorizationQuiBim': 'QuiBim '+token}
    connection.request("GET", "/api/subject/"+subjectId, payload, headers)
    res = connection.getresponse()
    httpStatusCode = res.status
    msg = res.read()  # whole response must be readed in order to do more requests using the same connection
    if httpStatusCode != 200:
        print('Error downloading eForm. HttpStatusCode: %d %s' % (httpStatusCode, res.reason))
        print(msg)
        return None
    
    print('ok')
    try:
        return json.loads(msg)
    except Exception as e:
        print('Error parsing JSON response message: "'+msg.decode('utf-8')+'"')
        return dict()

    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='This script opens a JSON file with an array of selected studies and generated from Quibim Precision '
                                               + 'and downloads the eForm objects for each unique subject of the studies. '
                                               + 'Then creates an output directory with a symbolic link for each of the selected studies pointing to '
                                               + 'the "real" study directory in the Quibim Precision working directory, '
                                               + 'and finally writes all eForms downloaded in a file named "'+EFORMS_FILE_NAME+'".',
                                    epilog='Example of use: \n'
                                               + '  python '+os.path.basename(__file__)+' --input input.json --qp-host "primage.quibim.com" --user quibimUser@example.com '
                                               + '--output-dir output --qp-working-dir /mnt/oneclient/PRIMAGE_DATALAKE')
    parser.add_argument('--input', type=str, required=True, help='Input JSON file with an array of studies selected')
    parser.add_argument('--qp-host', type=str, required=True, help='Quibim Precision hostname')
    parser.add_argument('--qp-port', type=str, help='Quibim Precision port')
    parser.add_argument('--no-tls', action='store_true', help='Use only HTTP, without TLS (insecure, for debugging purposes)')
    parser.add_argument('--no-check-cert', action='store_true', help='No validate the certificate when using TLS (insecure, for debugging purposes)')
    parser.add_argument('--user', type=str, required=True, help='Username or email for the login')
    parser.add_argument('--password', type=str, default='..........', help='Password for the login')
    parser.add_argument('--output-dir', type=str, required=True, help='Output directory where symbolic links and eforms.json file will be created.')
    parser.add_argument('--qp-working-dir', type=str, required=True, help='Path where the Quibim Precision working directory has been mounted.')
    parser.add_argument('--filter-by-list', type=str, default='..........', help='Path to a file containing the selected studies. One studyId per line.')
    parser.add_argument('--filter-by-masks-dir', action='store_true', help='Select only series with "masks" directory.')
    args = parser.parse_args()
    
    if not os.path.exists(args.input): 
        print('Error: input file not found: ' + args.input)
        exit(code=1)
    with open(args.input, 'r') as inputStream:
        selectedStudies = json.loads(inputStream.read())

    if args.filter_by_list != '..........': 
        if not os.path.exists(args.input): 
            print('Error: filter-by-list file not found: ' + args.filter_by_list)
            exit(code=1)
        with open(args.filter_by_list, 'r') as inputStream:
            filter = {}
            for line in inputStream:
                studyId = line.rstrip()
                filter[studyId] = 1
        filteredStudies = []
        for study in selectedStudies:
            studyId = study['studyId']
            if study['studyId'] in filter:
                filteredStudies.append(study)
        selectedStudies = filteredStudies

    if args.no_tls:
        connection = http.client.HTTPConnection(args.qp_host, args.qp_port) 
    else:
        context = ssl._create_unverified_context() if args.no_check_cert else None
        connection = http.client.HTTPSConnection(args.qp_host, args.qp_port, context=context)

    password = args.password
    if password == '..........':
        password = getpass()

    token = login(connection, args.user, password)
    if token=='': exit(code=1)

    output = {}
    for study in selectedStudies:
        subjectId = study['subjectId']
        if not isinstance(subjectId, str):
            try:
                subjectId = study['subjectId']['_id']
            except Exception as e:
                print('Warning: unknown subject '+ str(study['subjectId']) + ' for study '+ str(study['studyId']))
                continue
        if subjectId in output: continue
        print('Downloading subject '+subjectId+'... ', end='')
        ret = downloadSubject(subjectId, connection, token)
        if ret==None:
            exit(code=2)
        else:
            #print(ret)
            if not 'form' in ret.keys(): 
                print('Warning: subject '+ subjectId + ' without eForm.')
            else:
                form = ret['form']
                if int(form['status']['id']) < 2: print('Warning: subject '+ subjectId + ' with incomplete eForm (state: '+form['status']['state']+').')
                output[subjectId] = { 'subjectCode': ret['code'], 
                                      'path': study['folder'][1],
                                      'eForm': form }

    print('Creating output directory...')
    outDir = args.output_dir
    os.mkdir(outDir)

    print('Creating symbolic links...')
    for study in selectedStudies:
        userDirName = study['folder'][0]
        subjectDirName = study['folder'][1]
        studyDirName = study['folder'][2]
        subjectDirPath = os.path.join(outDir, subjectDirName)
        if not os.path.exists(subjectDirPath): os.mkdir(subjectDirPath)
        studyDirPath = os.path.join(subjectDirPath, studyDirName)
        if not args.filter_by_masks_dir:   # In this case the symlinks are at the level of studies.
            if os.path.islink(studyDirPath):
                print('Warning: there is a duplicated study or two studies with the same destination path ('+ studyDirPath + ').')
            else:
                os.symlink(os.path.join(args.qp_working_dir, userDirName, subjectDirName, studyDirName), studyDirPath, target_is_directory=True)
        else:   # args.filter_by_masks_dir
            # In this case the symlinks are at the level of series, because not all series will be selected.
            # This option is a bit slower because some requests to the remote file system are done for each study/series.
            # Thus, some prints are done to see the process of studies and series selected.
            if os.path.exists(studyDirPath): 
                print('\nWarning: there is a duplicated study or two studies with the same destination path ('+ studyDirPath + ').')
            else:
                os.mkdir(studyDirPath)
                print('(', end='', flush=True)

                for seriesDirName in os.listdir(os.path.join(args.qp_working_dir, userDirName, subjectDirName, studyDirName)):
                    seriesDirPath_inDatalake = os.path.join(args.qp_working_dir, userDirName, subjectDirName, studyDirName, seriesDirName)
                    if not os.path.isdir(seriesDirPath_inDatalake): continue
                    if args.filter_by_masks_dir and not 'masks' in os.listdir(seriesDirPath_inDatalake): print('.', end='', flush=True); continue
                    print('*', end='', flush=True)
                    seriesDirPath = os.path.join(studyDirPath, seriesDirName)
                    os.symlink(seriesDirPath_inDatalake, seriesDirPath, target_is_directory=True)
                print(')', end='', flush=True)
    print(' ', flush=True)

    print('Writing ' + EFORMS_FILE_NAME)
    with open(os.path.join(outDir, EFORMS_FILE_NAME) , 'w') as outputStream:
        json.dump(list(output.values()), outputStream)
            
    print('Done.')
    exit(code=0)

